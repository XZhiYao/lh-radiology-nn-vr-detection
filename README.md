# LH Radiology NN VR Detection

Repository for project Neural Network Based Object Detection of Anatomical Structure and Medical Artifacts in Virtual Reality.

We have designed a VR application for viewing endocscopy images from upper and lower GI. Using deep leanring model, we are able to add labels and bounding boxes appropriately. 

For training this model tensorflow was used. For classfication model a simple preTrained network was used. For the objecrt detection model, MobileNet SSD was used. For SemanticSegmentation a UNet model was used.

Pipeline involves reading the video samples and converting them into frames and ge the model inference based on it. 

For VR, We have followed two approaches. Approach One involved use of WebGL and designing WebXR Vr systems with local model inference.This approach was limited with memory restrictions of the handheld devices. Approach Two involved the use of remote model infrence and the video was rendered on the VR HMD using a VR system built using Unity XR.The second approach offers better and immerisve results than the former.

Steps to run the Unity Model

Unity based VR setup works with a remote VR server. From the server video is obtained and displayed on the Head Mounted Display
```
Install Unity 2019.4.21f1 (Version Tested)
git clone 
pip install -r requirements.txt
Add videoPlay in your Unity Projects using Unity Hub with the add Projects option
cd VideoServer
python main.py
```


Steps to run the Classifcation Dev Server

Classification development server is used for testing classification model performance before using in the web server. The dev server is developed using flask and TensorFlow
```
git clone
cd Classification
pip install -r requirements.txt
python app.py
```

Steps to run the VideoServer(Object Detection)

Object Detection server takes a video as an input and then returns video with bouding boxes as output. The videos are then displayed on the Head Mounted Display developed using Unity XR kit.
```
git clone
pip install -r requirements.txt
cd VideoServer
python main.py
```
Steps to run the VideoServer(Semantic Segmentation)

Segmentation Server takes an video as input and then return a video with the image mask as Output. The videos are then displayed on the Head Mounted Display developed using Unity XR kit.
```
git clone
pip install -r requirements.txt
cd VideoServer
python mainSemSeg.py
```


The pretrained model and the custom model are available in ``` Model-Store``` folder

For running the WebXR Server. Refer the Readme inside the ```webapp``` folder

```
cd webapp
```


## Folder Directory Structure

```
lh-radiology-nn-vr-detection/
├── Classification/
│   ├── SampleInference.ipynb
│   ├── app.py
│   ├── model.py
│   ├── modelConvert.py
│   ├── model_utils.py
│   ├── pretrained.py
│   ├── requirements.txt
│   ├── train.py
│   └── utils.py
├── Model-Store/
│   ├── ClassificationModel
│   ├── MobileNetSSD
│   ├── WebPOPO
│   ├── README.md
│   └── import_pb_to_tensorboard.py
├── ObjectDetection/
│   └── MobileNetSSD/
│       ├── MainNotebook/
│       │   └── Roboflow_tensorflow_object_detection_mobilenet_colab_changes.ipynb
│       ├── SavedModel/
│       │   └── saved_model.pb
│       ├── web_model/
│       │   ├── group1-shard1of5.bin
│       │   ├── group1-shard2of5.bin
│       │   ├── group1-shard3of5.bin
│       │   ├── group1-shard4of5.bin
│       │   ├── group1-shard5of5.bin
│       │   └── model.json
│       ├── polyps_label_map (2).pbtxt
│       ├── Roboflow_tensorflow_object_detection_mobilenet_colab.ipynb
│       └── saved_model.pb
├── SemanticSegmentation/
│   ├── Iter1.ipynb
│   ├── Iter2.ipynb
│   ├── Iter3.ipynb
│   ├── SimpleUnetModel.ipynb
│   ├── frozen_graph.pb
│   ├── frozen_graph.pbtxt
│   ├── img.png
│   ├── kvasir-seg-polyp-segmentation-using-unet-pytorch.ipynb
│   ├── main.py
│   ├── model.png
│   └── result.txt
├── VideoServer/
│   ├── static/
│   │   ├── output
│   │   └── uploads
│   ├── templates/
│   │   └── upload.html
│   ├── app.py
│   └── main.py
├── WebApp/
│   ├── public/
│   │   ├── favicon.ico
│   │   ├── index.html
│   │   ├── logo512.png
│   │   ├── manifest.json
│   │   └── robots.txt
│   ├── src/
│   │   ├── Models
│   │   ├── hooks
│   │   ├── scripts
│   │   ├── web_model
│   │   ├── App.cs
│   │   ├── App.js
│   │   ├── images.json
│   │   └── index.ts
│   ├── README.md
│   ├── package-lock.json
│   ├── package.json
│   └── tsconfig.json
├── videoPlay/
│   ├── Assets
│   ├── Library
│   ├── Packages
│   └── ProjectSettings
└── README.md
```

## Files

``` Classification ``` stores files for classification model

``` Model-Store ``` common model store for all three types Object detection, classification and segmentation

``` ObjectDetection ``` contains files for developement and testing of object detection network

``` SemanticSegmentation ``` contains files for developement and testing of Semantic Segmentation network

``` VideoServer  ``` contains files for the remote video server. It has files for both Object Detections  and Semantic Segmentation

``` WebApp ``` contains files for remote WebXR server. 

``` videoplay ``` contains unity project files for the XR project.


## Architecture

![Architecture](Architecture.png)

## Final Google Summer of Code Gist
[Gist](https://gist.github.com/sshivaditya2019/e766be3041e3685916a306b286ca71af)
