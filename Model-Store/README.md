# LH Radiology NN VR Detection
##Model Storage

This directory contains the trained model. This directory is essential for proper functioning of other parts of application like ```VideoServer``` or `WebApp`

For reading the output or input tensor types and shape use the `import_pb_to_tensorboard.py`.
