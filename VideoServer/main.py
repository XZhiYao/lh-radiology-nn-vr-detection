import os
from app import app
import urllib.request
from flask import Flask, flash, request, redirect, url_for, render_template
from werkzeug.utils import secure_filename
import numpy as np
from PIL import Image
import glob
import cv2 as cv
import matplotlib.pyplot as plt
import tensorflow.compat.v1 as tf
tf.disable_v2_behavior()
import cv2

@app.route('/')
def upload_form():
	return render_template('upload.html')

@app.route('/', methods=['POST'])
def upload_video():
	if 'file' not in request.files:
		flash('No file part')
		return redirect(request.url)
	file = request.files['file']
	if file.filename == '':
		flash('No image selected for uploading')
		return redirect(request.url)
	else:
		filename = secure_filename(file.filename)
		file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
		print('upload_path: ' + app.config['UPLOAD_FOLDER'] + filename)
		with tf.gfile.FastGFile('/gsoc/Src/Custom-Model/lh-radiology-nn-vr-detection/Model-Store/MobileNetSSD/saved_model.pb', 'rb') as f:
			graph_def = tf.GraphDef() #Load TF graph
			graph_def.ParseFromString(f.read())
		cap = cv2.VideoCapture(app.config['UPLOAD_FOLDER'] + filename) #Load the uploaded video
		outs = cv2.VideoWriter('static/outputs/project.mp4',cv2.VideoWriter_fourcc(*'mp4v'), 19, (768,576)) #Write the video
		img_array = []
		try:
			if not os.path.exists('data'):
				os.makedirs('data')
		except OSError:
				print ('Error: Creating directory of data')
		currentFrame = 0
		with tf.Session() as sess:
			# Restore session
			sess.graph.as_default()
			tf.import_graph_def(graph_def, name='')
			
			while(cap.isOpened()):
				# Capture frame-by-frame
				ret, frame = cap.read()
				print(ret)
				if ret:
					img = frame
					rows = img.shape[0]
					cols = img.shape[1]
					inp = cv.resize(img, (300, 300))
					inp = inp[:, :, [2, 1, 0]]  # BGR2RGB

					# Run the model
					out = sess.run([sess.graph.get_tensor_by_name('num_detections:0'),
									sess.graph.get_tensor_by_name('detection_scores:0'),
									sess.graph.get_tensor_by_name('detection_boxes:0'),
									sess.graph.get_tensor_by_name('detection_classes:0')],
								feed_dict={'image_tensor:0': inp.reshape(1, inp.shape[0], inp.shape[1], 3)})

					# Visualize detected bounding boxes.
					num_detections = int(out[0][0])
					for i in range(num_detections):
						classId = int(out[3][0][i])
						score = float(out[1][0][i])
						bbox = [float(v) for v in out[2][0][i]]
						if score > 0.3:
							x = bbox[1] * cols
							y = bbox[0] * rows
							right = bbox[3] * cols
							bottom = bbox[2] * rows
							cv.rectangle(img, (int(x), int(y)), (int(right), int(bottom)), (125, 255, 51), thickness=2)

					# Saves image of the current frame in jpg file
					img = cv2.resize(img,(768,576))
					img_array.append(img)
					print(currentFrame,"Saved")
					# To stop duplicate images
					currentFrame += 1
				else:
					break
		for i in range(len(img_array)):
			outs.write(img_array[i])
			print(i)
		# When everything done, release the capture
		cap.release()
		outs.release()
		cv2.destroyAllWindows()
		flash('Video successfully uploaded and displayed below')
		return render_template('upload.html', filename=filename)

@app.route('/display/<filename>')
def display_video(filename):
	print('display_video filename: ' + filename)
	return redirect(url_for('static', filename='outputs/' + 'project.mp4'), code=301)

if __name__ == "__main__":
	app.run()
