import * as tf from '@tensorflow/tfjs';
import { GraphModel } from '@tensorflow/tfjs';
import "@tensorflow/tfjs-backend-webgl";
var config = require('../config.json')


export enum VideoToFramesMethod {
    fps,
    totalFrames
  }
  
  export class VideoToFrames {
    /**
     * Extracts frames from the video and returns them as an array of imageData
     * @param videoUrl url to the video file (html5 compatible format) eg: mp4
     * @param amount number of frames per second or total number of frames that you want to extract
     * @param type [fps, totalFrames] The method of extracting frames: Number of frames per second of video or the total number of frames acros the whole video duration. defaults to fps
     * Loads the extracted frames into TFJS model for drawing the bounding box on it. The model is loaded from the local server
     */
    public static getFrames(
      videoUrl: string,
      amount: number,
      type: VideoToFramesMethod = VideoToFramesMethod.fps
    ): Promise<string[]> {
      //getFrames method helps in retriving the frames for model infrence
      return new Promise(
        (
          resolve: (frames: string[]) => void,
          reject: (error: string) => void
        ) => {
          let frames: string[] = [];
          let canvas: HTMLCanvasElement = document.createElement("canvas"); //using DOM Create a Canavs
          let context: CanvasRenderingContext2D = canvas.getContext("2d")!;
          let duration: number;
          let model:  Promise<GraphModel> = this.ModelLoad(); //Load Model asynchronously
  
          let video = document.createElement("video");
          video.crossOrigin = "Anonymous";
          video.preload = "auto";
          let that = this;
          video.addEventListener("loadeddata", async function () {
            canvas.width = video.videoWidth;
            canvas.height = video.videoHeight;
            duration = video.duration;
  
            let totalFrames: number = amount;
            if (type === VideoToFramesMethod.fps) {
              totalFrames = duration * amount;
            }
            for (let time = 0; time < duration; time += duration / totalFrames) {
              frames.push(await that.getVideoFrame(video, context, canvas, model,time));
            }
            resolve(frames);
          });
          video.src = videoUrl;
          video.load();
        }
      );
    }
    
    private static async ModelLoad(){
      const model = await tf.loadGraphModel('https://'+config['localip']+':8080/src/scripts/web_model/model.json') //Load Local Model need to start https server
      return model
    }
  
    private static getVideoFrame(
      video: HTMLVideoElement,
      context: CanvasRenderingContext2D,
      canvas: HTMLCanvasElement,
      model: Promise<GraphModel>,
      time: number
    ): Promise<string> {
      return new Promise(
        (resolve: (frame: string) => void, reject: (error: string) => void) => {
          let eventCallback = () => {
            video.removeEventListener("seeked", eventCallback);
            
            this.storeFrame(video, context, canvas, model,resolve);
          };
          video.addEventListener("seeked", eventCallback);
          video.currentTime = time;
        }
      );
    }
    

    private static async inferRe(
        model: { executeAsync: (arg0: any) => any; },tf4d: any, context: CanvasRenderingContext2D,video: HTMLVideoElement)
        {
        let output = await model.executeAsync(tf4d); //Asynchronous model execution
        const data = []
        for (let i = 0; i < output.length; i++)
          data.push(output[i].arraySync())
        output = null
        for (let sd = 0; sd < data[1][0].length; sd++)
          if (data[1][0][sd] > 0.80){
              tf4d.dispose()
              var [ymin, xmin, ymax, xmax] = data[3][0][sd]
              console.log(xmin*video.videoWidth, xmax*video.videoWidth, ymin*video.videoHeight, ymax*video.videoHeight)
          }
          console.log(tf.memory()) //Refer console log for Memory Consumption
          context.beginPath();  
          context.lineWidth = 10;
          context.rect(xmin*video.videoWidth,ymin*video.videoHeight, xmax*video.videoWidth-xmin*video.videoWidth,ymax*video.videoHeight-ymin*video.videoHeight);
          context.stroke();
          
          return [ymin, xmin, ymax, xmax]
      }
  
    private static storeFrame(
      video: HTMLVideoElement,
      context: CanvasRenderingContext2D,
      canvas: HTMLCanvasElement,
      model: Promise<GraphModel>,
      resolve: (frame: string) => void
    ) {
        const tfimg = tf.browser.fromPixels(video).toInt();
        const smallImg = tf.image.resizeBilinear(tfimg, [72,72]);
        tfimg.dispose() //Dispose Tensors after usage
        const resized = tf.cast(smallImg, 'int32');
        smallImg.dispose() //Dispose Tensors after usage
        var tf4d_ = tf.tensor4d(Array.from(resized.dataSync()), [1,72, 72, 3]); 
        const tf4d = tf.cast(tf4d_, 'int32');
        tf4d_.dispose() //Dispose Tensors after usage
        context.drawImage(video, 0, 0, video.videoWidth, video.videoHeight);
        model.then(async (model)=>{
            const rems = await this.inferRe(model,tf4d,context,video)
            resolve(canvas.toDataURL()); //Async Canavs Resolve
            
        })
        
     
        
    }
  }
