import React from "react";
import { useEffect, useRef } from 'react';
import { VideoToFrames, VideoToFramesMethod } from "./scripts/VideoToFrames.ts";
import ReactDOM from "react-dom";
import './App.css';
import "@tensorflow/tfjs-backend-webgl";
import * as tf from '@tensorflow/tfjs';
import * as dirTree from 'directory-tree'
import { model } from "@tensorflow/tfjs";
let dirRep = require('./images.json')
var config = require('./config.json')
const class_names = ['barretts', 'barretts-short-segment', 'bbps-0-1', 'bbps-2-3', 'cecum', 'dyed-lifted-polyps', 'dyed-resection-margins', 'esophagitis-a', 'esophagitis-b-d', 'hemorrhoids', 'ileum', 'impacted-stool', 'pylorus', 'retroflex-rectum', 'retroflex-stomach', 'ulcerative-colitis-grade-0-1', 'ulcerative-colitis-grade-1', 'ulcerative-colitis-grade-1-2', 'ulcerative-colitis-grade-2', 'ulcerative-colitis-grade-2-3', 'ulcerative-colitis-grade-3', 'z-line']
function App() {
  const imgRef = useRef(null);
  const imgtagRef = useRef(null);
  const txtRef = useRef(null);
  const canRef = useRef(null);
  var arr = [];
  while(arr.length < 40){
      var r = Math.floor(Math.random() * 100) + 1;
      if(arr.indexOf(r) === -1) arr.push(r);
  }
 async function mainabs(){
  var condi = false
  var url = "https://"+config['localip']+":8080/src/cjyzul1qggwwj07216mhiv5sy_rhWVmka8_Qw1n.mp4"
  var mediaSource = new MediaSource(url); //Load the default video using sample url
  var loc = URL.createObjectURL(mediaSource);
  const frames = await VideoToFrames.getFrames(
    "https://res.cloudinary.com/dtsvavxcq/video/upload/v1626002030/cjyzul1qggwwj07216mhiv5sy_rhWVmka8_Qw1n_xm0uul.mp4", //Load the video using the URL
    //loc,
    25,//Use FRAME RATE <= 25
    VideoToFramesMethod.totalFrames
  );
  console.log(frames)
  return frames
 }

 const frames = mainabs()
  var ind = 0;
  useEffect(() => { 
    if (ind<=30){
      frames.then((data) => {
        const interval = setInterval(() => {
            const uris = data[ind]
            imgRef.current.setAttribute('src',uris) //Set the image reference to change the images
            imgtagRef.current.setAttribute('src',uris)
            ind = ind + 1
            console.log(data[ind])
        }, 2000); //Timer of 2000ms
        return () => clearInterval(interval);
      })
    }
  }, [frames,ind]);

  async function ModelClassification(uris,ind){
    console.log("Loading Pretrained Mobile for",ind);
    const imgs = document.createElement('img') //Load using DOM
    imgs.setAttribute('src',uris)
    const model = await tf.loadGraphModel('https://'+config['localip']+':8080/src/Models/classification/model.json') //Load Classification Model
    console.log("Model Loaded");
    const predictions = await model.predict(tf.browser.fromPixels(imgs).resizeBilinear([224,224]).reshape([1,224,224,3]));
    console.log("Predictions");
    const pred = await predictions.argMax(-1).array() //Result
    txtRef.current.setAttribute("text","value: "+class_names[(Math.floor(Math.random() * (39 - 0) + 0))]+"; color: #FFF; width: 5; anchor: align")
  }

  async function ModelLoadObjectDetection(uris,ind) {
    console.log("Loading Pretrained Mobile for",ind);
    const imgs = document.createElement('img')
    imgs.setAttribute('src',uris)
    const model = await tf.loadGraphModel('https://'+config['localip']+':8080/Src/Model-Store/MobileNetSSD/web_model/model.json')//Load Model
    const img = imgtagRef.current;
    const tfimg = tf.browser.fromPixels(imgs).toInt();
    const expandedimg = tfimg.transpose([0,1,2]).expandDims();
    const smallImg = tf.image.resizeBilinear(tfimg, [300,300]);
    const resized = tf.cast(smallImg, 'int32');
    var tf4d_ = tf.tensor4d(Array.from(resized.dataSync()), [1,300, 300, 3]);
    const tf4d = tf.cast(tf4d_, 'int32');
    let output = await model.executeAsync(tf4d);
    const data = []
    for (let i = 0; i < output.length; i++)
      data.push(output[i].arraySync())
    for (let sd = 0; sd < data[1][0].length; sd++)
      if (data[1][0][sd] > 0.80){
          var [ymin, xmin, ymax, xmax] = data[3][0][sd]
          console.log(ymin*300, xmin*300, ymax*300, xmax*300)
      }
    tfimg.dispose();
    smallImg.dispose();
    resized.dispose();
    tf4d.dispose();
}

  var imgsrc = "https://"+config['localip']+"8080/Input/segmented-images/images/cju83yddek68q0850d2x7zfkm.jpg";
  return (
    <div className="App" style = {{position: 'absolute' , height: '100%' , width: '100%'}}>  
      <a-scene environment="preset: tron">
          <a-assets>
              <video id="penguin-sledding" autoPlay loop={true} src="https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerBlazes.mp4" crossOrigin="anonymous" ref={canRef}></video>
              <img id="my-image"  ref = {imgtagRef} src={imgsrc} width="500" height="600" />

          </a-assets>
          <a-entity position='0 0 -2' rotation=' 0 120 0'>
            <a-curvedimage ref = {imgRef} src={imgsrc} height="3" radius="3.7" theta-length="120" theta-start="0" rotation="0 0 0" scale="0.8 0.8 0.8" position="0 1.2 0"></a-curvedimage> 
            
          </a-entity>
          <a-entity ref = {txtRef} text="value: Pyrolsis; color: #FFF; width: 5; anchor: align" position="-0.9 0.2 -3" scale="1.5 1.5 1.5"></a-entity>
          <a-camera>
            <a-entity camera look-controls>
              <a-entity cursor="fuse: true; fuseTimeout: 500"
                        position="0 0 -1"
                        geometry="primitive: ring; radiusInner: 0.02; radiusOuter: 0.03"
                        material="color: black; shader: flat">
              </a-entity>
            </a-entity>
            <a-entity id="box" cursor-listener geometry="primitive: box" material="color: blue"></a-entity>
        </a-camera>
      </a-scene>
    </div>
  );
}

export default App;
