#########
#Imports#
#########

import tensorflow as tf
import numpy as np
import os
import tensorflow_hub as hub
import model as md

 
 #######
 #Model#
 #######
 
class CollectBatchStats(tf.keras.callbacks.Callback):
  """ Custom  CallBack """
  def __init__(self):
    self.batch_losses = []
    self.batch_acc = []

  def on_train_batch_end(self, batch, logs=None):
    self.batch_losses.append(logs['loss'])
    self.batch_acc.append(logs['acc'])
    self.model.reset_metrics()



class preTrained():

    def __init__(self, class_size):
        super(preTrained, self).__init__()
        feature_extractor_model = "https://tfhub.dev/google/bit/s-r50x1/1"
        feature_extractor_layer = hub.KerasLayer(
        feature_extractor_model, input_shape=(224, 224, 3), trainable=False, output_shape=(None,1280))
        global_average_layer = tf.keras.layers.GlobalAveragePooling2D()
        self.model = tf.keras.Sequential([
        feature_extractor_layer,
        tf.keras.layers.Dense(class_size)
        ])
        self.model.build([None, 224, 224, 3])  # Batch input shape.
        self.model.compile(
        optimizer=tf.keras.optimizers.Adam(),
        loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
        metrics=['acc',md.Main_Model.f1_m,md.Main_Model.precision_m,md.Main_Model.recall_m])  

    def model_ret(self):
        return self.model