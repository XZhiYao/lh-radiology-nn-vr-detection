#########
#Imports#
#########

import tensorflow as tf
import numpy as np
import os
from utils import KvasirLabelImage
import model as md
import pretrained as pdm
import tensorflow_hub as hub
import argparse
import datetime


##########
#Training#
##########

PATH = os.path.dirname(os.path.dirname(os.path.dirname(os.getcwd()))) + '/Input/' #PATH OF Dataset

model = md.Main_Model(22, 224, 224, 0.001)

data = KvasirLabelImage('/gsoc/Input/labeled-images/Images', 224, 224, 1, 0.2)


""" num_classes = 22
learning_rate = 0.001
training_steps = 2000
batch_size = 1
display_step = 1
model_b = model.mod_retturn() """ # For Using Native Gradient Tape Training 


""" for step, (batch_x, batch_y) in enumerate(data.train_ds.take(training_steps), 1):

    model.run_optimization(model_b, batch_x, batch_y, model.optimizer)
    if step % display_step == 0:
        pred = model_b(batch_x)
        loss = model.cross_entropy_loss(pred, batch_y)
        acc = model.accuracy(pred, batch_y)
        print("step: %i, loss: %f, accuracy: %f" % (step, loss, acc))  """ # For trainable var Training



def CustomModel():
    """ 
        Used for creating a custom model
    """
    log_dir = "logs/fit/" + 'CustomModel'
    tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)
    callback = tf.keras.callbacks.EarlyStopping(monitor='loss', patience=3)
    cusMod = model.main_mo_ret()
    cusMod.compile(
    optimizer=tf.keras.optimizers.RMSprop(lr=0.0001, decay=1e-6),
    loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
    metrics=['accuracy',model.f1_m,model.precision_m,model.recall_m]) 
    history = cusMod.fit(data.train_ds, epochs=15,callbacks=[callback,tensorboard_callback],validation_data=data.val_ds)
    cusMod.save('model/CustomModel',save_format='tf')

###################
#Transfer Learning#
###################


def pretrain():
    """ 
        For Use a Pre Trained Network and performing transfer learning
    """
    log_dirs = "logs/fit/" + 'PreTrain'
    tensorboard_callbacs = tf.keras.callbacks.TensorBoard(log_dir=log_dirs, histogram_freq=1)
    callback = tf.keras.callbacks.EarlyStopping(monitor='loss', patience=3)
    setpdm  = pdm.preTrained(len(data.train_ds.class_names))
    batch_stats_callback = pdm.CollectBatchStats()
    history = setpdm.model.fit(data.train_ds, epochs=5,
                        callbacks=[batch_stats_callback,callback,tensorboard_callbacs],validation_data=data.val_ds)
    setpdm.model.save('model/preTrain',save_format='tf')
   

############
#Arg Parser#
############

parser = argparse.ArgumentParser()
parser.add_argument('--type', type=str, required=False)
args = parser.parse_args()

if (args.type == 'Custom'):
    CustomModel()
elif (args.type == 'Pre'):
    pretrain() 
elif (args.type == 'Both'):
    CustomModel()
    pretrain()
