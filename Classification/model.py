#########
#Imports#
#########

import numpy as np
from sklearn.metrics import roc_auc_score
import tensorflow as tf
from tensorflow.keras import layers, Model
import tensorflow_addons as tfa
from tensorflow.keras import backend as K


#######
#Model#
#######
class ConvNet(Model):
    '''
    ConvNet Model
    '''
    def __init__(self, class_size, image_height, image_width):

        super(ConvNet, self).__init__()
        self.image_height = image_height
        self.image_width = image_width
        self.class_size = class_size

        self.conv1 = layers.Conv2D(32, kernel_size = 5, activation = tf.nn.relu)
        self.maxpool1 = layers.MaxPool2D(3, strides = 2)
        self.bn1 = layers.BatchNormalization()
        self.conv2 = layers.Conv2D(64, kernel_size = 5, activation = tf.nn.relu)
        self.maxpool2 = layers.MaxPool2D(3, strides = 2)
        self.conv3 = layers.Conv2D(128, kernel_size = 5, activation = tf.nn.relu)
        self.maxpool3 = layers.MaxPool2D(3, strides = 2)
        self.bn3 = layers.BatchNormalization()
        self.conv4 = layers.Conv2D(256, kernel_size = 5, activation = tf.nn.relu)
        self.maxpool4 = layers.MaxPool2D(3, strides = 2)
        self.conv5 = layers.Conv2D(512, kernel_size = 5, activation = tf.nn.relu)
        self.maxpool5 = layers.MaxPool2D(3, strides = 2)
        self.bn5 = layers.BatchNormalization()
        self.flatten = layers.Flatten()
        self.fc1 = layers.Dense(512)
        self.dropout = layers.Dropout(0.3)
        self.out = layers.Dense(class_size)

    def call(self, x, is_training = False):

        x = tf.reshape(x, [-1, 224, 224, 3])
        x = self.conv1(x)
        x = self.maxpool1(x)
        x = self.conv2(x)
        x = self.maxpool2(x)
        x = self.conv3(x)
        x = self.maxpool3(x)
        x = self.conv4(x)
        x = self.maxpool4(x)
        x = self.conv5(x)
        x = self.maxpool5(x)
        x = self.bn5(x)
        x = self.flatten(x)
        x = self.fc1(x)
        x = self.dropout(x, training=is_training)
        x = self.out(x)

        if not is_training:
            x = tf.nn.softmax(x)
        return x

class Main_Model():
    '''
        Custom CNN model for Kvasir Dataset
    '''
    def __init__(self, class_size, image_height, image_width, learning_rate, **kwargs):
        super(Main_Model, self).__init__()
        self.class_size = class_size
        self.image_height = image_height
        self.image_width = image_width
        self.model = ConvNet(class_size, image_height, image_width)
        self.learning_rate = learning_rate
        self.optimizer = tf.keras.optimizers.RMSprop(lr=0.0001, decay=1e-6)
        self.train_loss = tf.keras.metrics.Mean('train_loss', dtype=tf.float32)
        self.train_accuracy = tf.keras.metrics.SparseCategoricalAccuracy('train_accuracy')
        self.test_loss = tf.keras.metrics.Mean('test_loss', dtype=tf.float32)
        self.test_accuracy = tf.keras.metrics.SparseCategoricalAccuracy('test_accuracy')
                

    @classmethod
    def main_check(cls,model):
        if cls.model is None:
            raise ValueError('Model to be used cannot be empty')
    @classmethod   
    def cross_entropy_loss(cls, x, y):
        """ Used to find the Cross Entropy Loss """
        cls.y = tf.cast(y, tf.int64)
        cls.loss = tf.nn.sparse_softmax_cross_entropy_with_logits(labels = y, logits = x)
        return tf.reduce_mean(cls.loss)

    @classmethod
    def accuracy(cls, y_pred, y_true):
        """ Accuracy """
        cls.correct_prediction = tf.equal(tf.argmax(y_pred, -1), tf.cast(y_true, tf.int64))
        return tf.reduce_mean(tf.cast(cls.correct_prediction, tf.float32), axis = -1)

    @classmethod
    def run_optimization(cls, model, x, y, optimizer):
        """ For Accesing the gradient tape based model training """
        with tf.GradientTape() as g:
            cls.pred = model(x, is_training = True)
            cls.loss = cls.cross_entropy_loss(cls.pred, y)
        
        cls.trainable_variables = model.trainable_variables
        cls.gradients = g.gradient(cls.loss, cls.trainable_variables)
        optimizer.apply_gradients(zip(cls.gradients, cls.trainable_variables))

    def main_mo_ret(self):
        """ Returns a built model which can be trained using fit method """
        self.model.build((None, self.image_height, self.image_width, 3))
        return self.model
    
    def mod_retturn(self):
        return self.model


    @classmethod
    def recall_m(cls,y_true, y_pred):
        true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
        possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
        recall = true_positives / (possible_positives + K.epsilon())
        return recall

    @classmethod
    def precision_m(cls, y_true, y_pred):
        true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
        predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
        precision = true_positives / (predicted_positives + K.epsilon())
        return precision

    @classmethod
    def f1_m(cls, y_true, y_pred):
        true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
        predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
        possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
        precision = true_positives / (predicted_positives + K.epsilon())
        recall = true_positives / (possible_positives + K.epsilon())
        return 2*((precision*recall)/(precision+recall+K.epsilon()))
    


