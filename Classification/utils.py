#########
#Imports#
#########


import os
import numpy as np
import tensorflow as tf
import warnings

##############
#Dataset main#
##############


class KvasirLabelImage():
    '''
        A class for building the Kvasir dataset generator in tensorflow
    '''

    def __init__(self ,data_dir ,image_height ,image_width ,batch_size, validation_split = 0.2):
        '''
        Args:
                data_dir (String): Directory of the Image folder
                image_height (Integer): Height of an Image
                image_width (Integer): Width of an Image
                batch_size (Integer): Size of the Batch
                validation_split (Float): A float value for validation split
        '''
        super(KvasirLabelImage, self).__init__()
        self.error_chk(data_dir ,image_height ,image_width ,batch_size)
        self.train_ds = tf.keras.preprocessing.image_dataset_from_directory(
        data_dir,
        validation_split=validation_split,
        subset="training",
        shuffle=True,
        seed=123,
        label_mode = "int",
        image_size=(image_height, image_width),
        batch_size=batch_size)
        self.val_ds = tf.keras.preprocessing.image_dataset_from_directory(
        data_dir,
        validation_split=0.2,
        subset="validation",
        seed=123,
        shuffle=True,
        label_mode = "int",
        image_size=(image_height, image_width),
        batch_size=batch_size)

    def train_dataset(self):
        return self.train_ds
    
    def val_dataset(self):
        return self.val_ds

    def cnames(self):
        return self.train_ds.class_names

    def error_chk(self ,data_dir ,image_height ,image_width ,batch_size):
        '''
        Checks for errors or any other issues and raises warnings appropriately
        Args:
                data_dir (String): Directory of the Image folder
                image_height (Integer): Height of an Image
                image_width (Integer): Width of an Image
                batch_size (Integer): Size of the Batch
        '''

        if data_dir is None:
            raise ValueError("Input Directory cannot be empty")
        
        if image_height < 224:
            warnings.warn('Standard Image height for many pre-trained networks is 224')
        
        if image_width < 224:
            warnings.warn('Standard Image width for many pre-trained networks is 224')

        if batch_size > 129:
            warnings.warn('Batch Size too large might exceed memory')

        