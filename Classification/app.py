#########
#Imports#
#########
import os
from flask import Flask, request, redirect, url_for, jsonify, Response
from werkzeug.utils import secure_filename
import tensorflow as tf
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input, decode_predictions
from tensorflow.keras.preprocessing import image
import numpy as np
from PIL import Image
import io

class_names = ['barretts', 'barretts-short-segment', 'bbps-0-1', 'bbps-2-3', 'cecum', 'dyed-lifted-polyps', 'dyed-resection-margins', 'esophagitis-a', 'esophagitis-b-d', 'hemorrhoids', 'ileum', 'impacted-stool', 'pylorus', 'retroflex-rectum', 'retroflex-stomach', 'ulcerative-colitis-grade-0-1', 'ulcerative-colitis-grade-1', 'ulcerative-colitis-grade-1-2', 'ulcerative-colitis-grade-2', 'ulcerative-colitis-grade-2-3', 'ulcerative-colitis-grade-3', 'z-line']
class_names = np.array(class_names)
#######
#Flask#
#######
app = Flask(__name__)
model_custom = None
model_pre = None

def load_model():
    """ Load Model """
    global model_custom
    global model_pre
    model_custom = tf.keras.models.load_model('model/CustomModel')
    model_pre = tf.keras.models.load_model('model/preTrain')


#Predict Route e.g localhost:5000/predict
@app.route('/predict/custom', methods=['GET', 'POST'])
def upload_file_cus():
    """ For Uploading the File """
    response = {'success': False}
    if request.method == 'POST':
        if request.files.get('file'): # image is stored as name "file"
            img_requested = request.files['file'].read()
            img = Image.open(io.BytesIO(img_requested))
            if img.mode != 'RGB':
                img = img.convert('RGB')
            img = img.resize((224, 224))
            img = image.img_to_array(img)
            img = np.expand_dims(img, axis=0)
            inputs = preprocess_input(img)
            preds = model_custom.predict(inputs)
            predicted_id = np.argmax(preds, axis=-1)
            predicted_label_batch = class_names[predicted_id]
            response['predictions'] = [predicted_label_batch.tolist()]
            response['success'] = True
            return jsonify(response)

    return '''
    <!doctype html>
    <title>Upload new File</title>
    <h1>Upload new File</h1>
    <form method=post enctype=multipart/form-data>
      <p><input type=file name=file>
         <input type=submit value=Upload>
    </form>
    '''
@app.route('/predict/pre', methods=['GET', 'POST'])
def upload_file_pre():
    """ For Uploading the File """
    response = {'success': False}
    if request.method == 'POST':
        if request.files.get('file'): # image is stored as name "file"
            img_requested = request.files['file'].read()
            img = Image.open(io.BytesIO(img_requested))
            if img.mode != 'RGB':
                img = img.convert('RGB')
            img = img.resize((224, 224))
            img = image.img_to_array(img)
            img = np.expand_dims(img, axis=0)
            inputs = preprocess_input(img)

            preds = model_pre.predict(inputs)
            predicted_id = np.argmax(preds, axis=-1)
            predicted_label_batch = class_names[predicted_id]
            response['predictions'] = [predicted_label_batch.tolist()]
            response['success'] = True
            return jsonify(response)

    return '''
    <!doctype html>
    <title>Upload new File</title>
    <h1>Upload new File</h1>
    <form method=post enctype=multipart/form-data>
      <p><input type=file name=file>
         <input type=submit value=Upload>
    </form>
    '''

if __name__ == '__main__':
    load_model()
    app.run(threaded=False)


